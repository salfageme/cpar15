/**
 * Computación Paralela - Curso 2014/2015
 *
 * Práctica3 : Multiplicación de matrices MPI
 *
 * @author Samuel Alfageme, Pablo García - grupo 35
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "cputils.h"

//#define PRINT
#define TAM 1000

double a[TAM][TAM];
double b[TAM][TAM];
double c[TAM][TAM];

int main(int nargs, char** vargs) {

	int rank, nprocs;

	MPI_Init(&nargs, &vargs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	int i, j, k; 
	int checksum=0;
	double inicio, fin;


	if(rank==0){

		for (i=0;i<TAM;i++)
			for (j=0;j<TAM;j++)
				a[i][j]=0.5+(i+j)/TAM;

		for (i=0;i<TAM;i++)
			for (j=0;j<TAM;j++)
				b[i][j]=1.0+(i+j)/TAM;

		inicio=cp_Wtime();
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) {
			c[i][j]=0;
			for (k=0;k<TAM;k++)
				c[i][j] += a[i][k]*b[k][j];
			}
		}
		fin=cp_Wtime();

#ifdef PRINT
		printf("Resultado seq:\n");
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
#endif 
		for (i=0;i<TAM;i++) 
			for (j=0;j<TAM;j++) 
				checksum = (checksum + (int) c[i][j]) % 999999;

		printf("Checksum seq: %d\n",checksum);
		printf("Time seq: %lf\n",(fin-inicio));
	}

	/* ======= MPI ========================================================== */

	if(rank==0){

		// Se reinicia el Checksum para que con 1 sólo procesador no sea el mismo
		for (i=0;i<TAM;i++)
			for (j=0;j<TAM;j++)
				c[i][j] = 0;

		inicio=cp_Wtime();

		int particiones = nprocs - 1;
		int p;

		for(p=0;p<particiones;p++){

			int p_ini = p * TAM / particiones;
			int p_fin = ((p + 1) * TAM / particiones)-1;
			int tam = p_fin - p_ini + 1;

			MPI_Send(&tam,1,MPI_INT,p+1,999,MPI_COMM_WORLD);
			MPI_Send(&p_ini,1,MPI_INT,p+1,999,MPI_COMM_WORLD);

			MPI_Send(a[p_ini],TAM*tam,MPI_DOUBLE,p+1,999,MPI_COMM_WORLD);

			MPI_Send(b,TAM*TAM,MPI_DOUBLE,p+1,999,MPI_COMM_WORLD);

		}

		// Se reciben las secciones de la matriz C en cualquier orden:
		for(p=0;p<particiones;p++){
			int p_ini, tam;
			MPI_Status stat; 
			// Extraemos el stat.MPI_SOURCE para evitar que se reciba de un proceso diferente alguno de los datos
			MPI_Recv(&tam, 1, MPI_INT, MPI_ANY_SOURCE, 999, MPI_COMM_WORLD, &stat);
			MPI_Recv(&p_ini, 1, MPI_INT, stat.MPI_SOURCE, 999, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(c[p_ini],TAM*tam,MPI_DOUBLE,stat.MPI_SOURCE,888,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}

		fin=cp_Wtime();

#ifdef PRINT
		printf("Final:\n"); 
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
		fflush(stdout);
#endif 
		checksum=0;
		for (i=0;i<TAM;i++) 
			for (j=0;j<TAM;j++) 
				checksum = (checksum + (int) c[i][j]) % 999999;

		printf("Checksum: %d\n",checksum);
		printf("Time: %lf\n",(fin-inicio));

	} else {

		int tam, p_ini;
		MPI_Recv(&tam, 1, MPI_INT, 0, 999, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&p_ini, 1, MPI_INT, 0, 999, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("rank %d recibe partición de tamaño %d que empieza en %d\n",rank,tam,p_ini);	

		MPI_Recv(a,TAM*tam,MPI_DOUBLE,0,999,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		MPI_Recv(b,TAM*TAM,MPI_DOUBLE,0,999,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		for (i=0;i<tam;i++) {       
			for (j=0;j<TAM;j++) {     
			c[i][j]=0;
			for (k=0;k<TAM;k++)
				c[i][j] += a[i][k]*b[k][j];
			}
		}

#ifdef PRINT
		printf("Resultado parcial de %d:\n",rank);
		for (i=0;i<tam;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
		fflush(stdout);
#endif 
		// Envío de los datos necesarios para deshacer la partición:
		MPI_Send(&tam,1,MPI_INT,0,999,MPI_COMM_WORLD);
		MPI_Send(&p_ini,1,MPI_INT,0,999,MPI_COMM_WORLD);
		MPI_Send(c,TAM*tam,MPI_DOUBLE,0,888,MPI_COMM_WORLD);
	}


	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
