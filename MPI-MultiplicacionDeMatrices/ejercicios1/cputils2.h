/**
 * Computación Paralela
 * Funciones para las prácticas
 * 
 * @author Javier Fresno
 *
 */
#ifndef _CPUTILS2_
#define _CPUTILS2_

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Cargar MPI si el macro está definido
#ifdef CP_MPI
#include <mpi.h>
#endif


/*
 * VARIABLES
 */

// Flag para la semilla
int cp_rand_initialized = 0;


/*
 * FUNCIONES
 */


/*
 * Función para dormir el proceso
 */
inline void cp_msleep(int msec){

	if(msec < 0) return;

	struct timespec ts = {0, 0};
	struct timespec ts2 = {0, 0};

	long tv_sec = msec / 1000;
	long tv_nsec = (msec % 1000) * 1000000;

	ts2.tv_sec =  tv_sec;
	ts2.tv_nsec = tv_nsec;

	while(ts2.tv_nsec > 0 || ts2.tv_sec > 0){
		ts = ts2;
		if (nanosleep(&ts,&ts2) >= 0){
			return;
		}
	}
}




/**
 * Función que devuelve un número aleatorio
 */
inline int cp_rand(int num_min, int num_max){

	if(num_max < num_min) return 0;

	if(!cp_rand_initialized){
		int rank = 0;
#ifdef CP_MPI
		int flag;
		MPI_Initialized(&flag);
		if(flag) MPI_Comm_rank(MPI_COMM_WORLD, &rank);

#endif

		srand((unsigned) rank + (unsigned) time(NULL));
		cp_rand_initialized = 1;
	}

	return (num_min + rand() / (RAND_MAX / (num_max - num_min + 1) + 1));
}




char * cp_array2str(int * array, int len){

	static char cadena[1024*10];
	char * ptr = cadena;

	ptr += sprintf(ptr,"[");

	int i=0;	
	for(i=0; i<len; i++){

		ptr += sprintf(ptr,"%2d",array[i]);
		if(i<len-1)	ptr += sprintf(ptr,",");

	}
	sprintf(ptr,"]");

	return cadena;
}




#endif
