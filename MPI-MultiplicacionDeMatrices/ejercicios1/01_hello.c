/**
 * Computacion Paralela
 * Programa Hello World. 
 * 
 * @author Javier Fresno
 *
 */

// Librerías estándar.
#include <stdio.h>
#include <stdlib.h>

// Fichero de cabecera de MPI.
#include <mpi.h>

/**
 * Función main.
 */
int main(int argc, char *argv[]){

	// Definición de las variables para el rank y número de procesos.
	int rank, size;

	// Inicialización del entorno MPI.
	MPI_Init(&argc, &argv);

	// Obtenemos el identificador y el número de procesos.
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// 	Nombre del procesador
	char name[MPI_MAX_PROCESSOR_NAME];
	int name_size;
	MPI_Get_processor_name(name,&name_size);

	// Mensaje con la información.
	printf("Hi, I'm %d of %d @ %s MPI %d.%d\n",rank,size,name,MPI_VERSION,MPI_SUBVERSION);


	// Finalización en entorno MPI.
	MPI_Finalize();

	// Valor devuelto por el programa.
	return EXIT_SUCCESS;
}
