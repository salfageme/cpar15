/**
 * Computación Paralela
 *
 * Deadlock 
 *
 */

#include <stdio.h>
#include <mpi.h>

int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;
	int data;
	int tag = 0;
	MPI_Status stat;

	// 2. Iniciar MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// 3. Calcular el destino
	int dest = (rank + 1) % size;

	// 4. Recepción
	printf( "Process %d waiting recv\n", rank);
	MPI_Recv( &data, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat );
	printf( "Process %d received from %d\n", rank, stat.MPI_SOURCE);

	data = 123;

	// 5. Envío de la información
	printf( "Process %d sending to %d\n", rank, dest);
	MPI_Send( &data, 1, MPI_INT,           dest, tag, MPI_COMM_WORLD );


	// 6. Finalizar MPI.
	MPI_Finalize();
	return 0;
}
