/**
 * Computacion Paralela
 * 
 * @author Javier Fresno
 *
 */

// Librerías estándar.
#include <stdio.h>
#include <stdlib.h>


#define TAM 3

/**
 * Función main.
 */
int main(int argc, char *argv[]){

	int i,j;

	int matrix1[TAM][TAM] = {1,2,3,4,5,6,7,8,9};
	int matrix2[ TAM*TAM] = {1,2,3,4,5,6,7,8,9};
	int * matrix3 = malloc(sizeof(int) * (size_t) (TAM*TAM) );
	for(i=0; i<(TAM*TAM); i++) matrix3[i] = i+1;



#define m3(i,j) (matrix3[ (i) * TAM + (j) ])

	int ok = 1;
	for(i=0; i<TAM; i++)
		for(j=0; j<TAM; j++)
			if(matrix1[i][j] != matrix2[i*TAM+j] || matrix1[i][j] != m3(i,j) )
				ok = 0;
		
#undef m3

	printf("OK=%d\n",ok);


	// Valor devuelto por el programa.
	return EXIT_SUCCESS;
}
