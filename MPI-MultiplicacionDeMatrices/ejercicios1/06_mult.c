/**
 * Computación Paralela
 *
 * Multiplicación de matrices MPI
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "cputils.h"

//#define PRINT
#define TAM 1000

// Definimos las tres matrices

// Lo voy a dejar así para el ejemplo pero ESTO ESTÁ PROHIBIDO EN MPI: 
// Hay que usar matrices dinámicas con malloc porque si se hace así
// todos los procesos van a tener una copia de estas matrices aunque no
// las utilicen.
double a[TAM][TAM];
double b[TAM][TAM];
double c[TAM][TAM];


// Main
int main(int nargs, char** vargs) {

	// Mi identificador y el número de procesos
	int rank, nprocs;

	// Iniciar MPI
	MPI_Init(&nargs, &vargs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	int i, j, k; // Indices de bucles
	int checksum=0;
	double inicio, fin;


	if(rank==0){

		// Inicializaciones de A y B
		for (i=0;i<TAM;i++)
			for (j=0;j<TAM;j++)
				a[i][j]=0.5+(i+j)/TAM;

		for (i=0;i<TAM;i++)
			for (j=0;j<TAM;j++)
				b[i][j]=1.0+(i+j)/TAM;

		// Producto en secuencial para comparar tiempos
		inicio=cp_Wtime();
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) {
			c[i][j]=0;
			for (k=0;k<TAM;k++)
				c[i][j] += a[i][k]*b[k][j];
			}
		}
		fin=cp_Wtime();

		// Se muestra el resultado por pantalla
#ifdef PRINT
		printf("Resultado seq:\n");
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
#endif 


		// Se calcula y muestra el checksum
		for (i=0;i<TAM;i++) 
			for (j=0;j<TAM;j++) 
				checksum = (checksum + (int) c[i][j]) % 999999;

		printf("Checksum seq: %d\n",checksum);
		printf("Time seq: %lf\n",(fin-inicio));


	} // Fin the versión secuencial




	// Ahora MPI
	// --------------------------------------- //

	// El rank 0 va a ser el master (solo él tiene las matrices)
	// y le va a comunicar un trozo a los otros procesos
	// A se parte for filas y B se envía entera
	if(rank==0){

		// Inicio del tiempo, partición es parte del algoritmo
		inicio=cp_Wtime();

		int particiones = nprocs - 1;
		int p;

		for(p=0;p<particiones;p++){

			// Calculamos la partición
			// Filas de a, columnas de b
			int p_ini = p * TAM / particiones;
			int p_fin = ((p + 1) * TAM / particiones)-1;
			int tam = p_fin - p_ini + 1;

			// Primero voy a enviar el tamaño de la partición
			// Lo envio al rank p+1
			MPI_Send(&tam,1,MPI_INT,p+1,999,MPI_COMM_WORLD);

			// Envío también el inicio (primera fila de A y primera columna de B)
			MPI_Send(&p_ini,1,MPI_INT,p+1,999,MPI_COMM_WORLD);

			// Ahora envío las columnas de A
			// Estan contiguas en memoria, es muy fácil
			MPI_Send(a[p_ini],TAM*tam,MPI_DOUBLE,p+1,999,MPI_COMM_WORLD);

			// Ahora envío B
			// Usar un send no es buena opción, ya lo veremos en la parte
			// comunciaciones colectivas (broadcast).
			MPI_Send(b,TAM*TAM,MPI_DOUBLE,p+1,999,MPI_COMM_WORLD);


			// Recibo la solución
			// Funciona pero está mal hecho,
			// si envío y recibo en la misma iteración del bucle,
			// estoy secuancializando el problema.
			// ¿Para qué me molesto en partir si se procesa en secuencial?
			MPI_Recv(c[p_ini],TAM*tam,MPI_DOUBLE,p+1,888,MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		}


		// Fin
		fin=cp_Wtime();

		// Se muestra el resultado por pantalla
#ifdef PRINT
		printf("Final:\n"); 
		for (i=0;i<TAM;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
		fflush(stdout);
#endif 


		// Se calcula y muestra el checksum
		checksum=0;
		for (i=0;i<TAM;i++) 
			for (j=0;j<TAM;j++) 
				checksum = (checksum + (int) c[i][j]) % 999999;

		printf("Checksum: %d\n",checksum);
		printf("Time: %lf\n",(fin-inicio));




	// Código de otro proceso (rank !=0)
	} else {


		// Soy uno de los procesos que recibe de rank == 0
		
		// Primero leo el tamaño que me ha tocado
		// y cúal es mi primera fila de A
		int tam, p_ini;
		MPI_Recv(&tam, 1, MPI_INT, 0, 999, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&p_ini, 1, MPI_INT, 0, 999, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("rank %d recibe partición de tamaño %d que empieza en %d\n",rank,tam,p_ini);	

		// Recibo las columnas de A
		// Las pongo al inicio de A, no en su posición porque da igual sobra
		// espacio en A.
		// Cómo dije arriba, este proceso no debería tener una matriz A completa
		// sino solo el número de filas justo
		MPI_Recv(a,TAM*tam,MPI_DOUBLE,0,999,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		// Recibo B entero
		MPI_Recv(b,TAM*TAM,MPI_DOUBLE,0,999,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		// Producto secuencial
		// Ahora puedo hacer mi parte
		for (i=0;i<tam;i++) {          // Tenemos solo tam filas
			for (j=0;j<TAM;j++) {     
			c[i][j]=0;
			for (k=0;k<TAM;k++)
				c[i][j] += a[i][k]*b[k][j];
			}
		}

		// Se muestra el resultado por pantalla
#ifdef PRINT
		printf("Resultado parcial de %d:\n",rank);
		for (i=0;i<tam;i++) {
			for (j=0;j<TAM;j++) 
				printf("%5.1lf",c[i][j]);
			printf("\n");
		}
		fflush(stdout);
#endif 
		// Envío mi parte a rank==0
		MPI_Send(c,TAM*tam,MPI_DOUBLE,0,888,MPI_COMM_WORLD);
	}


	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
