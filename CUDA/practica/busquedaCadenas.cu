/**
 * Computación Paralela - Curso 2014/2015
 *
 * Práctica5 : Búsqueda de Cadenas CUDA
 *
 * @author Samuel Alfageme, Pablo García - grupo 35
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "cputils.h"
#include "mac.h"
#include "kernel_busqueda.cu"

#define CUDA_DEVICE 0

#define THREADS_PER_BLOCK 256

int main(int argc, char *argv[]) {

    double inicio, fin, tiempoAcumulado=0;  // Contadores de tiempo.
    size_t sizeB, *sizeS;                   // Para almacenar los tamaños de los ficheros.
    char  *B,   **S;                        // Para almacenar las cadenas.
    int *h_B, **h_S;                        // Para almacenar las cadenas como int.
    int i,j;
    int tss=0;                              // Tamaño total de todas las cadenas S_i
    cudaError_t error;

    //*****************************************************************************************

    cudaSetDevice(CUDA_DEVICE);             // Selección de una de las tarjetas TITAN de portal

    //*****************************************************************************************
    // Vamos a trabajar como si cada char fuera un int, mapeamos los clásicos B y S a h_B y h_S

    // Lectura de chars y posterior procesado a int
    string_reading

    // Conversion de B a h_B
    h_B = (int*) malloc(sizeof(int)*(int)sizeB);    for(i=0; i<sizeB; i++) h_B[i] = (int) B[i];

    // Conversion de S a h_S
    h_S = (int**) malloc( (int)(argc-2)*sizeof(int*) );
    for(i=0; i<(argc-2); i++){
        h_S[i] = (int*) malloc( sizeof(int) * sizeS[i] );
        for(j=0; j<sizeS[i]; j++)  h_S[i][j] = (int) S[i][j]; 
    }

    // Liberacion de recursos que ya no vamos a usar
    free(B); for (i=0;i<(argc-2);i++) free(S[i]);   


    // *****************************************************************************************

    /*Declaración del shape de los bloques y del grid */

    int nPartsB = sizeB / THREADS_PER_BLOCK;

    dim3 gridconf(nPartsB);

    // Teniendo en cuenta el tamaño de la cadena B, la memoria compartida será ~512B
    int sharedMemorySize = (sizeB * sizeof(int))/ nPartsB;

    int *d_B;
    int *d_S;
    int *h_resultado, *d_resultado;

    h_resultado = (int*) malloc(sizeof(int));

    cudaMalloc((void**) &d_B, sizeof(int)*sizeB);
    
    inicio=cp_Wtime();
    cudaMemcpy(d_B, h_B, sizeof(int)*sizeB, cudaMemcpyHostToDevice);
    fin=cp_Wtime();
    tiempoAcumulado+=fin-inicio;

    /* 2. BUSQUEDA */
    for (i=0;i<(argc-2);i++) {

        *h_resultado = INT_MAX;

        cudaMalloc((void**) &d_S, sizeof(int)*sizeS[i]);

        inicio=cp_Wtime();

        cudaMemcpy(d_S, h_S[i], sizeof(int)*sizeS[i], cudaMemcpyHostToDevice);

        cudaMalloc((void**) &d_resultado, (int) sizeof(int));

        cudaMemcpy(d_resultado, h_resultado, sizeof(int), cudaMemcpyHostToDevice);

        gpu_FuncBusqueda<<<gridconf, THREADS_PER_BLOCK, sharedMemorySize>>>(d_B, sizeB, d_S, sizeS[i], d_resultado);

        cudaMemcpy(h_resultado, d_resultado, sizeof(int), cudaMemcpyDeviceToHost);

        fin=cp_Wtime(); tiempoAcumulado+=fin-inicio; 
        
        // Impresión del resultado:
        printf("Result: %d\n", *h_resultado == INT_MAX ? -1 : *h_resultado);
    }

    printf("Time: %lf\n",tiempoAcumulado);

    /* 3. FREEs */
    free(h_B); for (i=0;i<(argc-2);i++) free(h_S[i]); free(sizeS);

    /* 4. LIBERAR LA MEMORIA DEL DEVICE */
    cudaFree(d_B);
    cudaFree(d_S);

    /* 5. LIBERAR LA GPU RESERVADA CON cudaSetDevice()  */
    cudaDeviceReset();

    /* 6. FIN */
    return 0;
}
