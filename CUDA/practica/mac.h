#define string_reading \
	if ( argc < 3 ) { 											\
		fprintf( stderr, "\nFormato: %s <cad-B> <cad-S0> {<cad-Si>} \n\n", argv[0] );			\
		exit( EXIT_FAILURE );										\
	}													\
	sizeS = (size_t*) malloc( (size_t) (argc-2)*sizeof(size_t));						\
	S = (char**) malloc( (size_t) (argc-2)*sizeof(char*));							\
	if ( ! cp_FileSize(argv[1],&sizeB) ) {									\
		fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[1] );				\
		exit( EXIT_FAILURE );										\
	}													\
	B = (char*) malloc( sizeB );										\
	if ( B == NULL ) {											\
		fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeB );		\
		exit( EXIT_FAILURE );										\
	}													\
	if ( ! cp_FileRead(argv[1],B) ) {									\
		fprintf( stderr, "Error: En lectura de %s\n", argv[1] );					\
		exit( EXIT_FAILURE );										\
	}													\
	for (i=0;i<(argc-2);i++) {										\
		if ( ! cp_FileSize(argv[i+2],&(sizeS[i])) ) {							\
			fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[i+2] );			\
			exit( EXIT_FAILURE );									\
		}												\
		S[i] = (char*)malloc( sizeS[i] );								\
		tss += sizeS[i];										\
		if ( S[i] == NULL ) {										\
			fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeS[i] );	\
			exit( EXIT_FAILURE );									\
		}												\
		if ( ! cp_FileRead(argv[i+2],S[i]) ) {								\
			fprintf( stderr, "Error: En lectura de %s\n", argv[i+2] );				\
			exit( EXIT_FAILURE );									\
		}												\
	}													

int busqueda( int *S, int sizeS, int *B, int sizeB ) {
        int result = -1; // Resultado de la busqueda

        /* 1. PARA CADA POSICION DE COMIENZO EN B (ultima posible sizeB-sizeS) */
        int startB;
        for( startB = 0; result==-1 && startB <= sizeB-sizeS ; startB++ ) {
                int ind;
                /* 2. PARA CADA POSICION DE S */
                for( ind = 0; ind<sizeS; ind++ ) {
                        /* 2.1. CARACTER NO COINCIDENTE, PASAR A SIGUIENTE POSICION DE B */
                        if ( S[ind] != B[startB+ind] ) break;
                }
                /* 3. COMPROBAR CONDICION DE SALIDA DEL BUCLE */
                if ( ind == sizeS ) result = startB;
        }
        return result;
}

