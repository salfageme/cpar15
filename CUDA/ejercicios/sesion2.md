Samuel Alfageme Saiz,Pablo García Sanz
Grupo : 35


E4:

4.1
	
	a) 	arrayHost = (int*) malloc(sizeof(int) * ELEMENTS);
	
	
	b)	cudaMalloc( (void**) &arrayDevice, sizeof(int) * (int) ELEMENTS);
		
	c)	cudaMemcpy(arrayDevice,arrayHost, sizeof(int)ELEMENTS,cudaMemcpyHostToDevice);
	
	d)	kernel_A<<<1,ELEMENTS>>>(arrayDevice);
	
	e)	cudaMemcpy(arrayHost,arrayDevice,sizeof(int)*ELEMENTS,cudaMemcpyDeviceToHost);

4.3
	Cada hilo de la GPU escribe en la posición correspondiente del array de datos la columna que ocupa dentro del bloque

4.4 	int indice = trow*ELEMENTS+tcolumn;

	array[indice] = tcolumn;

	if(indice < ELEMENTS/2)
		array[indice] = array[indice+1]*100+array[indice];
	if(indice > ELEMENTS/2)
		array[indice] = array[indice-1]*100-array[indice];

4.5	No, no es necesaria sincronización explícita puesto que los movimientos de datos son síncronos.

4.7	Que podría haber un problema de competencia por uno de los elementos del array entre varios hilos

E5:

5.1	Comprobamos que efectivamente, el resultado tras la ejecución no varía.

5.2 Salida : 100 201 2 197 296

5.3 Si no se pusiera cudaDevicedSyncronize, podría imprimir los valores del array antes de que este hubiera hecho los cálculos correspondientes y/o que los kernels terminaran de realizar las operaciones tras la finalización de la CPU.

E6

6.1	Según la sección 3.2.5.3 de la Cuda Toolkit Documentation: "The maximum number of kernel launches that a device can execute concurrently is 16 on devices of compute capability 2.0 through 3.0; the maximum is 32 concurrent kernel launches on devices of compute capability 3.5 and higher. Devices of compute capability 3.2 are limited to 4 concurrent kernel launches."

6.2	No estaba instalado CUDA en nuestras máquinas locales.

6.3 Portal en la cola ( cudaslurm ) tiene una arquitectura 3.5 por lo que puede llegar a soportar 32 kernels concurrentemente.
