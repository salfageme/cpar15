#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <cuda.h>
#include <unistd.h>
#include "kernel.cu"

/*Definición de constantes*/
#define currentGPU 0		//El número más alto suele indicar la salida de vídeo
#define ITEMS 1572864 	//Longitud del array	


/*Declaración de funciones  CPU*/
void vector_ini(int *A, int *B, int *C);	//Inicialización de los vectores
void test(int *A, int *B, int *C);	//Corrección del ejercicio

int main(void)
{

	/*Declaración e inicialización de variables CPU (HOST)*/
	int *A;
	int *B;
	int *C;

	/*Indicamos la GPU (DEVICE) que vamos a utilizar*/
	int *dA;
	int *dB;
	int *dC;

	/*Reserva de memoria para de variables CPU*/
	if ((A=(int *) calloc(ITEMS, sizeof(int)))==NULL){		//vector A
		printf("error\n");
		exit (-1);
	}
	if ((B=(int *) calloc(ITEMS, sizeof(int)))==NULL){		//vector B
		printf("error\n");
		exit (-1);
	}
	if ((C=(int *) calloc(ITEMS, sizeof(int)))==NULL){		//vector C
		printf("error\n");
		exit (-1);
	}


	/*Reserva de memoria para variables del DEVICE (en memoria GPU)*/
	cudaMalloc( (void**) &dA, sizeof(int)*ITEMS);
	cudaMalloc( (void**) &dB, sizeof(int)*ITEMS);
	cudaMalloc( (void**) &dC, sizeof(int)*ITEMS);

	/*Inicialización de las matrices*/
	vector_ini(A, B, C);

	/*Copia de datos del HOST al DEVICE*/
	cudaMemcpy(dA, A, sizeof(int)*ITEMS, cudaMemcpyHostToDevice);
	cudaMemcpy(dB, B, sizeof(int)*ITEMS, cudaMemcpyHostToDevice);

	/*Declaración del shape de los bloques y del grid (primer ejercicio)*/
	int hilos = 512;
	int gridrows=2;
	dim3 bloqShapeGpuFunc1(hilos,1);
	dim3 gridShapeGpuFunc1(ITEMS/(hilos*gridrows),gridrows);
	//dim3 gridShapeGpuFunc1(64,8);

cudaEvent_t start;
cudaEventCreate(&start);
cudaEvent_t stop;
cudaEventCreate(&stop);
cudaEventRecord(start, NULL);

	/*Lanzamos la función del DEVICE*/
	gpuFunc_vecAdd<<<gridShapeGpuFunc1, bloqShapeGpuFunc1>>>(dA, dB, dC);	
	cudaThreadSynchronize();

cudaEventRecord(stop, NULL);
cudaEventSynchronize(stop);
float msec = 0.0f;
cudaEventElapsedTime(&msec, start, stop);
printf("eventos!!!!!!!!!!! %.6f milisegundos\n", msec);


	/*Copia de datos del DEVICE al HOST*/
	cudaMemcpy(C, dC, sizeof(int)*ITEMS, cudaMemcpyDeviceToHost);

	/*Corrección del ejercicio*/
	test(A, B, C);

	/*Liberamos memoria del DEVICE*/
	cudaFree(dA);
	cudaFree(dB);
	cudaFree(dC);

	/*Liberamos memoria del HOST*/
	free(A);
	free(B);
	free(C);

	/*Liberamos los hilos del DEVICE*/
	cudaThreadExit();

} //main


/*Inicialización de las matrices */
void vector_ini(int *A, int *B, int *C){

	for(int i=0; i<ITEMS; i++){
		A[i]=i;
		B[i]=i+1;
		C[i]=0;
	}

}//vector_ini

/*Imprimimos los valores del vector (tantos como num_elements)*/
void print_vector(int *C, int num_elements) {

	for(int i=0; i<num_elements; i++){
		printf("%d ",C[i]);
	}
	printf("\n");

} //print_vector


/*Corrección del ejercicio*/
void test(int *A, int *B, int *C){

int error=0;

	for(int i=0; i<ITEMS; i++){
		if ((A[i] + B[i])!=C[i]) error=1;
	}
	
if (error==1) printf("\n****El ejercicio no es correcto (ERROR)\n\n");
else printf("\n****TODO CORRECTO****\n\n");

} //test


