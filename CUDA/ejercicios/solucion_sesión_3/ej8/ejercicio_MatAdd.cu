#include <stdio.h>
#include <cuda.h>
#include "kernel.cu"

/*Definición de constantes*/
#define currentGPU 0		//El número más alto suele indicar la salida de video
#define N 1024			//Dimensión de la matriz
#define M N


/*Declaración de funciones CPU*/
void matrix_ini(int *A, int *B);		//Inicialización de las matrices 
void test(int *A, int *B, int *C);		//Corrección del ejercicio


int main()
{

	/*Declaración e inicialización de variables CPU (HOST)*/
	int *A;
	int *B;
	int *C;

	/*Indicamos la GPU (DEVICE) que vamos a utilizar*/
	int *dA;
	int *dB;
	int *dC;

	/*Reserva de memoria para de variables CPU*/
	if ((A=(int *) calloc(N*M, sizeof(int)))==NULL){	//matriz A
					printf("error\n");
			exit (-1);
				}

	if ((B=(int *) calloc(N*M, sizeof(int)))==NULL){	//matriz B
					printf("error\n");
			exit (-1);
				}
				
	if ((C=(int *) calloc(N*M, sizeof(int)))==NULL){	//matriz C
					printf("error\n");
			exit (-1);
				}


	/*Reserva de memoria para las variables del DEVICE (en memoria GPU)*/
	cudaMalloc((void**)&dA, sizeof(int)*N*M);
	cudaMalloc((void**)&dB, sizeof(int)*N*M);
	cudaMalloc((void**)&dC, sizeof(int)*N*M);

	/*Inicialización de las matrices*/
	matrix_ini(A, B);

	/*Copia de datos del HOST al DEVICE*/
	cudaMemcpy(dA,A,sizeof(int)*N*M,cudaMemcpyHostToDevice);
	cudaMemcpy(dB,B,sizeof(int)*N*M,cudaMemcpyHostToDevice);

	/*Declaración del shape de los blosues y del grid */
	dim3 bloqShapeGpuFunc1(256,2);
	dim3 gridShapeGpuFunc1(4,512);

	/*Lanzamos la función del DEVICE*/
	gpuFunc_MatAdd<<<gridShapeGpuFunc1, bloqShapeGpuFunc1>>>(dA, dB, dC);

	/*Copia de datos del DEVICE al HOST*/
	cudaMemcpy(C,dC,sizeof(int)*N*M,cudaMemcpyDeviceToHost);

	/*Corrección del ejercicio*/
	test(A, B, C);

	/*Liberamos memoria del DEVICE*/
	cudaFree(dA);
	cudaFree(dB);
	cudaFree(dC);

	/*Liberamos memoria del HOST*/
	free(A);
	free(B);
	free(C);

	/*Liberamos los hilos del DEVICE*/
	cudaThreadExit();

} //main


/*Inicialización de las matrices */
void matrix_ini(int *A, int *B){

	for(int i=0; i<N; i++){
		for(int j=0; j< M; j++){
			A[i*N+j]=i;
			B[i*N+j]=i+j;
		}
	}

}//matrix_ini


/*Corrección del ejercicio*/
void test(int *A, int *B, int *C){
int error=0;
	for(int i=0; i<N; i++){
		for(int j=0; j<M; j++){
			if ((A[i*N+j] + B[i*N+j])!=C[i*N+j]) error=1;
		}
	}
if (error==1) printf("\n****El ejercicio no es correcto (ERROR)****\n\n");
else printf("****TODO CORRECTO****\n\n");
}//Corrección del ejercicio


