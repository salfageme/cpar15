/*********************************************
Función del device para la suma de matrices 
**********************************************/
__global__ void gpuFunc_MatAdd(int *A, int *B, int *C)
{
	/*Identificaciones necesarios*/
	int IDX_Thread		=	threadIdx.x;				//Identificación del hilo en la dimension x
	int IDY_Thread		=	threadIdx.y;				//Identificación del hilo en la dimension y

	int IDX_block		=	blockIdx.x;				//Identificación del bloque en la dimension x
	int IDY_block		=	blockIdx.y;				//Identificación del bloque en la dimension y

	int shapeBlock_X		=	blockDim.x;				//Números del bloques en la dimensión x
	int shapeBlock_Y		=	blockDim.y;				//Números del bloques en la dimensión y

	/*Número de filas o de columnas de la matriz*/
	int matrix_side		= 	gridDim.x * blockDim.x; //o gridDim.y* blockDim.y es lo mismo ya que N==M

	/*Fórmula para calcular la posición*/	//Posición del vector dependiendo del hilo y del bloque 
	int position		= (IDY_block*shapeBlock_Y + IDY_Thread) * matrix_side + IDX_block * shapeBlock_X + IDX_Thread;	

	/*Hacemos la suma correspondiente*/
	C[position] = A[position] + B[position];
}
