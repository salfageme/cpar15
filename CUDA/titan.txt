Caracteristicas Titan 01

Consta de una CPU Quad-core con una velocidad de reloj de 2,33 Ghz y
 una GPU Titan Black de 2880 CUDA cores que funcionan a una frecuencia de 980 Mhz.
 6GB de memoria del tipo GDDR5.

 *************************
 SI QUIERES PNER ESTO TAMBIEN
 *************************

Core Clock
Base 889 MHz
Boost 980 MHz
Memory Clock 7000 MHz
Memory Size 6144 MB GDDR5
Memory Bus 384 Bits
