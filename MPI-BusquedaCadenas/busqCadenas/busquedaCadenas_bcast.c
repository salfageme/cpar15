/**
 * Computaci�n Paralela - Curso 2014/2015
 *
 * Pr�ctica4 : B�squeda de Cadenas MPI
 *
 * @author Samuel Alfageme, Pablo Garc�a - grupo 35
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <mpi.h>

#include "cputils.h"

int busqueda( char *S, int sizeS, char *B, int sizeB ) {
	int result = INT_MAX;
	int startB;
	for( startB = 0; result==INT_MAX && startB <= sizeB-sizeS ; startB++ ) {
		int ind;
		for( ind = 0; ind<sizeS; ind++ ) {
			if ( S[ind] != B[startB+ind] ) break;
		}
		if ( ind == sizeS ) result = startB;
	}
	return result;	
}


int main(int argc, char *argv[]) {

	int rank, nprocs;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	double inicio,fin,tiempoAcumulado=0; 
	int sizeB, *sizeS; 
	char *B, **S;
	char *particionB;
	int *resultados;
	int offset = 0;

	// = MASTER ===============================================================
	// + S�lo �l lee los ficheros y comunica al resto:
	//		- Las cadenas de b�squeda
	//		- Los fragmentos de partici�n de la cadena sobre la que se realizan
	// ========================================================================
	if(rank == 0){

		if ( argc < 3 ) {
			fprintf( stderr, "\nFormato: %s <cad-B> <cad-S0> {<cad-Si>} \n\n", argv[0] );
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}

		sizeS = (size_t*) malloc( (size_t) (argc-2)*sizeof(size_t));
		S = (char**) malloc( (size_t) (argc-2)*sizeof(char*));

		if ( ! cp_FileSize(argv[1],&sizeB) ) {
			fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[1] );
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}

		B = (char *) malloc( (size_t)sizeB );
		if ( B == NULL ) {
			fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeB );
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}

		if ( ! cp_FileRead(argv[1],B) ) {
			fprintf( stderr, "Error: En lectura de %s\n", argv[1] );
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}
	 
		int i;
		for (i=0;i<(argc-2);i++) {

			if ( ! cp_FileSize(argv[i+2],(size_t)&(sizeS[i])) ) {
				fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[i+2] );
				MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}

			if(sizeS[i]>offset)
				offset = sizeS[i];

			S[i] = (char *)malloc( (size_t)sizeS[i] );
			if ( S[i] == NULL ) {
				fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeS[i] );
				MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}

			if ( ! cp_FileRead(argv[i+2],S[i]) ) {
				fprintf( stderr, "Error: En lectura de %s\n", argv[i+2] );
				MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}

		}


		// = ENV�OS ===========================================================

		inicio = MPI_Wtime();	

		// Retransmito el tama�o de la cadena que se va a partir, necesario para
		// los c�lculos del scatter:
		MPI_Bcast(&sizeB,1,MPI_INT,0,MPI_COMM_WORLD);
		// Los tama�os de las cadenas buscadas:
		MPI_Bcast(sizeS,argc-2,MPI_INT,0,MPI_COMM_WORLD);

		for(i=0;i<argc-2;i++){
			MPI_Bcast(S[i],sizeS[i],MPI_CHAR,0,MPI_COMM_WORLD);
		}

	} else {

		// El master ya tiene estos datos, el resto deber�n recibirlos:
		MPI_Bcast(&sizeB,1,MPI_INT,0,MPI_COMM_WORLD);
		sizeS = (size_t*) malloc( (size_t) (argc-2)*sizeof(size_t));
		MPI_Bcast(sizeS,argc-2,MPI_INT,0,MPI_COMM_WORLD);
		S = (char**) malloc( (size_t) (argc-2)*sizeof(char*));

		int i;
		for(i=0;i<argc-2;i++){
			S[i] = (char *)malloc( (size_t)sizeS[i] );
			MPI_Bcast(S[i],sizeS[i],MPI_CHAR,0,MPI_COMM_WORLD);
		}

		// = UNPACKING ========================================================
		for (i=0;i<(argc-2);i++) {
			// El tama�o de la mayor de las cadenas cortas determinar� el offset
			// que vamos a aplicar por defecto a las n-1 primeras particiones de
			// B para permitir solapamiento (no podemos suponer que todas tienen
			// el mismo tama�o siempre)
			if(sizeS[i]>offset)
				offset = sizeS[i];
		}
	}

	// = PARTICIONADO DE B ====================================================

	// Vectores necesarios para el scatter
	int * tam  = malloc(sizeof(int)*(size_t)nprocs);
	int * desp = malloc(sizeof(int)*(size_t)nprocs);

	// Si la divisi�n en particiones no es exacta, se a�ade al �ltimo fragmento 
	// lo necesario para cubrir toda la cadena 
	int extra = sizeB%nprocs == 0 ? 0 : sizeB - (sizeB/nprocs)*nprocs;
	
	int i;
	for(i=0;i<nprocs;i++){
		int solapamiento = i == nprocs-1 ? extra : offset-1;
		tam[i] = sizeB/nprocs + solapamiento;
		desp[i] = i*(sizeB/nprocs);
	}

	int particionB_size = tam[rank];
	particionB = malloc(sizeof(char)*(size_t)particionB_size);

	// = SCATTER ==============================================================

	MPI_Scatterv(
		B,tam,desp,MPI_CHAR,
		particionB,particionB_size,MPI_CHAR,
		0, MPI_COMM_WORLD
	);

	// = B�SQUEDAS ============================================================

	resultados = malloc( (argc-2)*sizeof(int) );
	int res_parcial;
	for(i=0;i<(argc-2);i++) {
		res_parcial = busqueda(S[i],sizeS[i],particionB,particionB_size);
		resultados[i] = res_parcial + (res_parcial == INT_MAX ? 0 : desp[rank]);
	}

	// = REDUCCI�N ============================================================

	int * minimos = malloc((size_t) (argc-2) * sizeof(int));
	MPI_Reduce(resultados, minimos, argc-2, MPI_INT, MPI_MIN,0,MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);
	fin = MPI_Wtime();
	tiempoAcumulado = fin - inicio;

	// = RESULTADOS ===========================================================

	if(rank == 0){
		for(i=0;i<argc-2;i++)
			printf("Result: %d\n", minimos[i] == INT_MAX ? -1 : minimos[i]);
		printf("Time: %lf\n",tiempoAcumulado);
	}

	// = FREEs ================================================================

	free(particionB);
	free(resultados);
	free(minimos);
	free(tam);
	free(desp);
	for(i=0;i<argc-2;i++)
		free(S[i]);
	if(rank==0)
		free(B);

	// = FIN ==================================================================
	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
