
#include <stdio.h>
#include <stdlib.h>

/**
 * Punto con tres coordenadas.
 */
typedef int Point[3];

/**
 * Struct con diferentes campos.
 */
typedef struct {
	Point point;
	double value;
	char type;
} Data;


/**
 * Main function
 */
int main( int argc, char *argv[] ){

	printf("Sizeof Data: %d\n", (int) sizeof(Data));
	
	return EXIT_SUCCESS;
}
