#include "cputils.h"
#include "cputils2.h"
#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]){

	int rank;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(rank == 0){
		int * data = malloc(sizeof(int)*3);
		int i;
		for(i=0; i<4; i++) data[i] = i;
		MPI_Send(data, 4, MPI_INT, 1, 555, MPI_COMM_WORLD);
	}

	if(rank == 1){
		int data[10] = {0,0,0,0,0,0,0,0,0,0};
		MPI_Recv(data, 10, MPI_INT, 0, 555, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf( "%s\n", cp_array2str(data,10));
	}

	MPI_Finalize();
	return 0;
}
