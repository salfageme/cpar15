/**
 * Computación Paralela
 * 
 * Sincronización y medición de tiempos.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "cputils.h"
#include "cputils2.h"


/**
 * Main function
 */
int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;
	double t_init, t_end, t_init_max;


	// 2. Iniciar MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// 3. Check number of processors
	if(size < 2){
		if(rank == 0) fprintf(stderr,"I need 2 processors at least\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// 4. Paramos el proceso entre 1 y 2 segundos
	cp_msleep(cp_rand(1000,2000));

	// 5. Hacemos un barrier midiendo los tiempos.
	t_init = MPI_Wtime();
	MPI_Barrier(MPI_COMM_WORLD);
	t_end = MPI_Wtime();


	// 5. All reduce para comprobar los tiempos de entrada y salida del barrier
	MPI_Allreduce(&t_init, &t_init_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

	printf("[%d] t_init: %6.4f, t_end: %6.4f, t_init_max: %6.4f, t_stop: %6.4f, t_end > t_init_max: %s\n",
		rank,t_init,t_end,t_init_max,t_end-t_init,(t_end > t_init_max ? "Yes" : "No" ));


	MPI_Finalize();
	return EXIT_SUCCESS;
}
