/**
 * Computación Paralela
 *
 * Operación Gather.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "cputils.h"
#include "cputils2.h"


/**
 * Main function
 */
int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;


	// 2. Iniciar MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// 3. Check number of processors
	if(size < 2){
		if(rank == 0) fprintf(stderr,"I need 2 processors at least\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// 4. Cada proceso inicia data aleatoriamente.
	int data = cp_rand(100,1000);
	printf("[%2d] I created: %d\n",rank, data );

	// 5. El proceso 0 reserva un array de tamaño size.
	int * data_in = NULL;
	if(rank == 0){
		data_in = malloc((size_t) size * sizeof(int));
	}

	// 6. Gatter.
    MPI_Gather(&data, 1, MPI_INT, data_in, 1, MPI_INT, 0, MPI_COMM_WORLD);

	// 7. El proceso 0 imprime el array.
	if(rank == 0){
		printf("[%2d] I collected: %s\n",rank, cp_array2str(data_in,size) );
	}

	MPI_Finalize();
	return EXIT_SUCCESS;
}
