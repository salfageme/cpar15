# Práctica 2 OpenMP - Búsqueda de Secuencias

## Algoritmo secuencial *naive*

```
int busqueda( char *S, int sizeS, char *B, int sizeB ) {
	int result = -1; // Resultado de la busqueda

	/* 1. PARA CADA POSICION DE COMIENZO EN B (ultima posible sizeB-sizeS) */ 
	int startB;
	for( startB = 0; result==-1 && startB <= sizeB-sizeS ; startB++ ) {
		int ind;
		/* 2. PARA CADA POSICION DE S */ 
		for( ind = 0; ind<sizeS; ind++ ) {
			/* 2.1. CARACTER NO COINCIDENTE, PASAR A SIGUIENTE POSICION DE B */
			if ( S[ind] != B[startB+ind] ) break;
		}
		/* 3. COMPROBAR CONDICION DE SALIDA DEL BUCLE */
		if ( ind == sizeS ) result = startB;
	}
	return result;	
}
```

## Ejecución:

* Primer juego de cadenas

```
$ ./busquedaCadenas CadB_1C CadS_5c_1 CadS_5c_2 CadS_5c_3
DEBUG Tam fichero grande: 100
DEBUG Tam fichero #0 (CadS_5c_1): 5
DEBUG Tam fichero #1 (CadS_5c_2): 5
DEBUG Tam fichero #2 (CadS_5c_3): 5
Result: 1
Result: 78
Result: -1
Time: 0.000002
```

* Segundo juego de cadenas

```
$./busquedaCadenas CadB_1M CadS_1K_1 CadS_1K_2 CadS_1K_3
DEBUG Tam fichero grande: 1000000
DEBUG Tam fichero #0 (CadS_1K_1): 1000
DEBUG Tam fichero #1 (CadS_1K_2): 1000
DEBUG Tam fichero #2 (CadS_1K_3): 1000
Result: 0
Result: 999000
Result: -1
Time: 0.003812
```

#### Geopar

```
$ client -u usuario -q openmp -t 4 busquedaCadenas.c CadB_100M_random CadS_1C_random_1 CadS_1C_random_2 CadS_1C_random_3 CadS_1C_random_4 CadS_1C_random_5
```

## Resultados:

| Request         | Time     | Average     | Speedup     |
|----------------:|---------:|------------:|------------:|
| Secuencial      |          |    2.172128 |           1 |
| 1207            | 2.172113 |             |             |
| 1208            | 2.172008 |             |             |
| 1209            | 2.172262 |             |             |
| 1 procesador    |          |    3.060033 |    0.326793 |
| 1210            |  3.05994 |             |             |
| 1211            | 3.060233 |             |             |
| 1212            | 3.059928 |             |             |
| 4 procesadores  |          |    1.319788 |    1.645815 |
| 1213            | 1.319919 |             |             |
| 1214            | 1.319642 |             |             |
| 1215            | 1.319803 |             |             |
| 8 procesadores  |          |    0.677309 |    3.206996 |
| 1216            |   0.6773 |             |             |
| 1217            | 0.677295 |             |             |
| 1218            | 0,677332 |             |             |
| 12 procesadores |          |    0.454633 |    4.777753 |
| 1219            |  0.45467 |             |             |
| 1220            | 0.454711 |             |             |
| 1221            |  0.45452 |             |             |

### Generación de la gráfica de *speedup*

```
$ gnuplot script.gnp > speedup-XX.eps
```

![grafica](http://i.imgur.com/NZp0gmj.jpg)

## Dependencias:

* [**client**][client]
* [gcc-4.9](https://gcc.gnu.org/gcc-4.9/)

[geopar]: http://geopar.infor.uva.es:8080/
[client]:http://geopar.infor.uva.es:8080/client